import { Controller, Get, Param, Header } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { CatsService } from './cats.service';
import { Cat } from './cat.interface';

@ApiTags('Cats')
@Controller()
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @ApiResponse({ status: 200, description: "Get all cats.", type: [Cat] })
  @Header('Content-Type', 'application/json')
  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @ApiResponse({ status: 200, description: "Get one cats.", type: Cat })
  @Header('Content-Type', 'application/json')
  @Get(':name')
  async findOne(
    @Param('name') name: string
  ): Promise<Cat> {
    return this.catsService.findOne(name);
  }
}