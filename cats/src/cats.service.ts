import { Injectable } from '@nestjs/common';
import { Cat } from './cat.interface';

@Injectable()
export class CatsService {
  private readonly cats: Cat[] = [
      {
          name: 'meow',
          age: 1,
          breed: 'foo'
      }
  ];

  create(cat: Cat) {
    this.cats.push(cat);
  }

  findOne(name: string): Cat {
    return this.cats.find(cat => cat.name === name) || {}
  }

  findAll(): Cat[] {
    return this.cats;
  }
}