import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { CatsModule } from './cats.module';

async function bootstrap(): Promise<void> {
  const PREFIX = 'cats';

  const app = await NestFactory.create<NestFastifyApplication>(
    CatsModule,
    new FastifyAdapter({
      trustProxy: true,
    }),
  );

  app.setGlobalPrefix(PREFIX)

  const options = new DocumentBuilder()
      .setTitle('Cats')
      .setDescription('A Cats API.')
      .setVersion('1.0')
      .addServer(PREFIX)
      .build()

  const document = SwaggerModule.createDocument(app, options, {
    ignoreGlobalPrefix: false,
  });

  SwaggerModule.setup('/docs', app, document, {});

  await app.listen(3000, '0.0.0.0');
}

void bootstrap();
