# NestJS Docker Compose and Nginx
A boilerplate with multiple dockerized apps exposed via Nginx on the same host: `http://localhost:8080`.


## Working
Exposes 2 api's both on `http://localhost:8080` but on different paths:
* /cats
* /dogs

### Start the app

```bash
docker-compose up
```

### Paths overview
* /cats/:name
* /cats/docs
* /dogs/:name
* /dogs/docs

