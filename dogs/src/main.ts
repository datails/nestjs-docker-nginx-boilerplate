import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { DogsModule } from './dogs.module';

async function bootstrap(): Promise<void> {
  const PREFIX = '/dogs';

  const app = await NestFactory.create<NestFastifyApplication>(
    DogsModule,
    new FastifyAdapter({
      trustProxy: true,
    }),
  );

  app.setGlobalPrefix(PREFIX)

  const options = new DocumentBuilder()
      .setTitle('Dogs')
      .setDescription('A Dogs API.')
      .setVersion('1.0')
      .addServer(PREFIX)
      .build()

  const document = SwaggerModule.createDocument(app, options, {
    ignoreGlobalPrefix: false,
  });

  SwaggerModule.setup('/docs', app, document, {});

  await app.listen(3000, '0.0.0.0');
}

void bootstrap();
