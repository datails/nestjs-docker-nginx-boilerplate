import { Injectable } from '@nestjs/common';
import { Dog } from './dog.interface';

@Injectable()
export class DogsService {
  private readonly dogs: Dog[] = [
      {
          name: 'woof',
          age: 1,
          breed: 'foo'
      }
  ];

  create(dog: Dog) {
    this.dogs.push(dog);
  }

  findOne(name: string): Dog {
    return this.dogs.find(dog => dog.name === name) || {};
  }

  findAll(): Dog[] {
    return this.dogs;
  }
}