import { Controller, Get, Param, Header } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { DogsService } from './dogs.service';
import { Dog } from './dog.interface';

@ApiTags('Dogs')
@Controller()
export class DogsController {
  constructor(private readonly dogsService: DogsService) {}

  @ApiResponse({ status: 200, description: "Get all dogs.", type: [Dog] })
  @Header('Content-Type', 'application/json')
  @Get()
  async findAll(): Promise<Dog[]> {
    return this.dogsService.findAll();
  }

  @ApiResponse({ status: 200, description: "Get one dog.", type: Dog })
  @Header('Content-Type', 'application/json')
  @Get(':name')
  async findOne(
    @Param('name') name: string
  ): Promise<Dog> {
    return this.dogsService.findOne(name);
  }
}